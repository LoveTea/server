//
// Created by kirill on 18.11.17.
//

#include <algorithm>
#include <unordered_set>

#include "HTTP.h"
#include "../utils/Strings.h"
#include "HTTPHeaderException.h"

namespace http {
	const std::unordered_set<std::string> HTTP::ALLOWED_REQUEST_TYPES { "GET" };
	const std::unordered_set<std::string> HTTP::ALLOWED_PROTOCOL_VERSIONS { "HTTP/1.0", "HTTP/1.1" };

	const std::unordered_map<std::string, void(*)(const HTTPRequest& request, HTTPResponse& response)> HTTP::HANDLERS {
		{ "/", &HTTPHandlers::root_handler },
		{ "/info", &HTTPHandlers::info_handler },
		{ "/image", &HTTPHandlers::image_handler }
	};

	HTTPRequest HTTP::parse_request(const char* cbegin, const char* cend) {
		HTTPRequest request;

		const char* iterator_begin = cbegin;
		auto iterator_end = std::find(cbegin, cend, '\n');
		if (iterator_end == cend || iterator_end == iterator_begin) {
			throw HTTPHeaderException("Invalid request format!");
		}
		std::string line(iterator_begin, iterator_end - 1);
		parse_starting_line(request, line);

		while (true) {
			iterator_begin = ++iterator_end;
			iterator_end = std::find(iterator_begin, cend, '\n');
			if (iterator_end == cend || iterator_end == iterator_begin) {
				throw HTTPHeaderException("Invalid request format!");
			}
			line.assign(iterator_begin, iterator_end - 1);
			if (line.empty()) {
				break;
			}
			parse_header(request, line);
		}
		++iterator_end;

		request.request_size = iterator_end - cbegin;

		if (request.headers.find("content-length") != request.headers.cend()) {
			long long int content_length = std::stoll(request.headers["content-length"]);
			if (content_length > cend - iterator_end) {
				throw HTTPException("Request's content not complete!");
			}
			request.content.assign(iterator_end, iterator_end + content_length);
			request.request_size += content_length;
		}

		return request;
	}

	void HTTP::parse_starting_line(HTTPRequest& request, const std::string& line) {
		std::string temp;
		auto iterator = line.cbegin();
		size_t position = line.find(' ');
		if (position == std::string::npos) {
			throw HTTPHeaderException("Invalid starting line!");
		}
		temp.assign(iterator, line.cbegin() + position);
		if (ALLOWED_REQUEST_TYPES.find(temp) == ALLOWED_REQUEST_TYPES.cend()) {
			throw HTTPHeaderException("Request type not allowed!");
		}
		request.request_type = temp;
		iterator = line.cbegin() + ++position;

		position = line.find(' ', position + 1);
		if (position == std::string::npos) {
			throw HTTPHeaderException("Invalid starting line!");
		}
		temp.assign(iterator, line.cbegin() + position);
		size_t params_position = temp.find('?');
		if (params_position != std::string::npos) {
			temp.assign(temp.cbegin(), temp.cbegin() + params_position);
		}
		if (temp.back() == '/' && temp.size() > 1) {
			temp.pop_back();
		}
		request.path = temp;
		iterator = line.cbegin() + ++position;

		temp.assign(iterator, line.cend());
		if (ALLOWED_PROTOCOL_VERSIONS.find(temp) == ALLOWED_PROTOCOL_VERSIONS.cend()) {
			throw HTTPHeaderException("Protocol version not allowed!");
		}
		request.protocol_version = temp;
	}

	void HTTP::parse_header(HTTPRequest& request, const std::string& line) {
		size_t position = line.find(':');
		if (position == std::string::npos) {
			throw HTTPHeaderException("Wrong header format!");
		}
		std::string key(line.cbegin(), line.cbegin() + position);
		utils::Strings::to_lower(key);
		std::string value(line.cbegin() + position + 1, line.cend());
		utils::Strings::trim(value);
		request.headers[key] = value;
	}

	HTTPResponse HTTP::not_found(HTTPResponse& response) {
		response.set_status_code(404);
		response.set_content("<h1>Not Found</h1>");

		return response;
	}

	HTTPResponse HTTP::server_error(HTTPResponse& response) {
		response.set_status_code(500);
		response.set_content("<h1>Internal Server Error</h1>");

		return response;
	}

	HTTPResponse HTTP::resolve(const HTTPRequest& request) {
		HTTPResponse response(200);

		if (request.protocol_version == "HTTP/1.0") {
			response.protocol_version = "HTTP/1.0";
			auto iterator = request.headers.find("connection");
			if (iterator == request.headers.cend() || utils::Strings::to_lower_copy(iterator->second) != "keep-alive") {
				response.connection_close = true;
				response.headers["connection"] = "close";
			}
		} else {
			auto iterator = request.headers.find("connection");
			if (iterator != request.headers.cend() && utils::Strings::to_lower_copy(iterator->second) == "close") {
				response.connection_close = true;
				response.headers["connection"] = "close";
			}
		}

		auto iterator = HANDLERS.find(request.path);
		if (iterator == HANDLERS.cend()) {
			return not_found(response);
		}

		try {
			iterator->second(request, response);
		} catch (const std::exception& error) {
			return server_error(response);
		}

		return response;
	}
}