//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_HTTP_H
#define SERVER_HTTP_H


#include <unordered_set>

#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "HTTPHandlers.h"
#include "../utils/Buffer.h"

namespace http {
	class HTTP {
		static const std::unordered_set<std::string> ALLOWED_REQUEST_TYPES;
		static const std::unordered_set<std::string> ALLOWED_PROTOCOL_VERSIONS;

		static const std::unordered_map<std::string, void(*)(const HTTPRequest& request, HTTPResponse& response)> HANDLERS;

		static void parse_starting_line(HTTPRequest& request, const std::string& line);
		static void parse_header(HTTPRequest& request, const std::string& line);

		static HTTPResponse not_found(HTTPResponse& response);
		static HTTPResponse server_error(HTTPResponse& response);

	public:
		static HTTPRequest parse_request(const char* cbegin, const char* cend);
		static HTTPResponse resolve(const HTTPRequest& request);
	};
}


#endif //SERVER_HTTP_H
