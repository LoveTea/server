//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_HTTPEXCEPTION_H
#define SERVER_HTTPEXCEPTION_H


#include "../utils/BaseException.h"

namespace http {
	class HTTPException : public utils::BaseException {
	public:
		explicit HTTPException(const std::string& message) : BaseException(message) {}
	};
}


#endif //SERVER_HTTPEXCEPTION_H
