//
// Created by kirill on 18.11.17.
//

#include <fstream>

#include "HTTPHandlers.h"
#include "HTTPException.h"

namespace http {
	void HTTPHandlers::root_handler(const HTTPRequest& request, HTTPResponse& response) {
		response.set_content("<h1>Super Mega Ultra Server!</h1><h6>Alpha version</h6>");
	}

	void HTTPHandlers::info_handler(const HTTPRequest& request, HTTPResponse& response) {
		response.set_content("<h1>About Super Mega Ultra Server!</h1><p>Maybe the best server in the world or maybe not.</p>");
	}

	void HTTPHandlers::image_handler(const HTTPRequest& request, HTTPResponse& response) {
		std::fstream file("cat.jpg", std::ios::in|std::ios::binary|std::ios::ate);
		if (!file.is_open()) {
			throw HTTPException("Image file not found!");
		}
		long int size = file.tellg();
		if (size == -1) {
			throw HTTPException("Image file read exception!");
		}
		std::string image(size, 0);
		file.seekg(0, std::ios::beg);
		file.read(&image[0], size);
		file.close();

		response.set_header("Content-Type", "image/jpg");
		response.set_content(std::move(image));
	}
}