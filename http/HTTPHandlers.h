//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_HTTPHANDLERS_H
#define SERVER_HTTPHANDLERS_H


#include "HTTPRequest.h"
#include "HTTPResponse.h"

namespace http {
	class HTTPHandlers {
	public:
		static void root_handler(const HTTPRequest& request, HTTPResponse& response);
		static void info_handler(const HTTPRequest& request, HTTPResponse& response);
		static void image_handler(const HTTPRequest& request, HTTPResponse& response);
	};
}


#endif //SERVER_HTTPHANDLERS_H
