//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_HTTPTYPEEXCEPTION_H
#define SERVER_HTTPTYPEEXCEPTION_H


#include "HTTPException.h"

namespace http {
	class HTTPHeaderException : public HTTPException {
	public:
		explicit HTTPHeaderException(const std::string& message) : HTTPException(message) {}
	};
}


#endif //SERVER_HTTPTYPEEXCEPTION_H
