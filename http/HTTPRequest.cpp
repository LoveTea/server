//
// Created by kirill on 18.11.17.
//

#include "HTTPRequest.h"

namespace http {
	const std::string& HTTPRequest::get_path() const {
		return path;
	}

	const std::string& HTTPRequest::get_request_type() const {
		return request_type;
	}

	const std::string& HTTPRequest::get_protocol_version() const {
		return protocol_version;
	}

	const std::unordered_map<std::string, std::string>& HTTPRequest::get_headers() const {
		return headers;
	}

	const std::string& HTTPRequest::get_content() const {
		return content;
	}

	uint64_t HTTPRequest::get_request_size() const {
		return request_size;
	}
}