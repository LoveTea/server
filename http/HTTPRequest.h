//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_HTTPREQUEST_H
#define SERVER_HTTPREQUEST_H


#include <vector>
#include <string>
#include <unordered_map>

namespace http {
	class HTTP;

	class HTTPRequest {
		friend class HTTP;

		std::string path;
		std::string request_type;
		std::string protocol_version;
		std::unordered_map<std::string, std::string> headers;
		std::string content;
		uint64_t request_size = 0;

		HTTPRequest() = default;

	public:
		const std::string& get_path() const;
		const std::string& get_request_type() const;
		const std::string& get_protocol_version() const;
		const std::unordered_map<std::string, std::string>& get_headers() const;
		const std::string& get_content() const;
		uint64_t get_request_size() const;
	};
}


#endif //SERVER_HTTPREQUEST_H
