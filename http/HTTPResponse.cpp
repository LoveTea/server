//
// Created by kirill on 18.11.17.
//

#include "HTTPResponse.h"
#include "HTTPException.h"
#include "../utils/Strings.h"

namespace http {
	const std::unordered_map<uint16_t, std::string> HTTPResponse::CODE_TO_REASON {
		{ 200, "OK" },
		{ 400, "Bad Request" },
		{ 404, "Not Found" },
		{ 500, "Internal Server Error" }
	};

	HTTPResponse::HTTPResponse(uint16_t status_code)
		: status_code(status_code) {
		auto iterator = CODE_TO_REASON.find(status_code);
		if (iterator == CODE_TO_REASON.cend()) {
			throw HTTPException("Wrong or not supported response status code!");
		}
		reason_phrase = iterator->second;
	}

	void HTTPResponse::set_status_code(uint16_t code) {
		auto iterator = CODE_TO_REASON.find(code);
		if (iterator == CODE_TO_REASON.cend()) {
			throw HTTPException("Wrong or not supported response status code!");
		}
		status_code = iterator->first;
		reason_phrase = iterator->second;
	}

	void HTTPResponse::set_header(const std::string& key, const std::string& value) {
		headers[utils::Strings::to_lower_copy(key)] = value;
	}

	void HTTPResponse::set_content(std::string&& string) {
		content.swap(string);
		headers["content-length"] = std::to_string(content.size());
	}

	void HTTPResponse::set_content(const char* cbegin, const char* cend) {
		content.assign(cbegin, cend);
		headers["content-length"] = std::to_string(content.size());
	}

	bool HTTPResponse::is_connection_close() const {
		return connection_close;
	}

	std::string HTTPResponse::to_string() const {
		std::string response;
		response.reserve(512 + content.size());
		response += protocol_version + " " +std::to_string(status_code) + " " + reason_phrase + "\n";
		for (const auto& header : headers) {
			response += header.first + ": " + header.second + "\r\n";
		}
		response += "\r\n";
		response.append(content.cbegin(), content.cend());

		return response;
	}
}