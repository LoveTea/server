//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_HTTPRESPONSE_H
#define SERVER_HTTPRESPONSE_H


#include <unordered_map>

namespace http {
	class HTTP;

	class HTTPResponse {
		friend class HTTP;

		static const std::unordered_map<std::uint16_t, std::string> CODE_TO_REASON;

		uint16_t status_code;
		std::string reason_phrase;
		std::string protocol_version{ "HTTP/1.1" };
		std::unordered_map<std::string, std::string> headers {
			{ "server", "Super server" },
			{ "content-language", "ru" },
			{ "content-type", "text/html; charset=utf-8" },
			{ "content-length", "0" },
			{ "connection", "Keep-Alive" }
		};
		std::string content;
		bool connection_close = false;

		explicit HTTPResponse(uint16_t status_code);

	public:
		void set_status_code(uint16_t code);
		void set_header(const std::string& key, const std::string& value);
		void set_content(std::string&& string);
		void set_content(const char* cbegin, const char* cend);
		bool is_connection_close() const;
		std::string to_string() const;
	};
}


#endif //SERVER_HTTPRESPONSE_H
