#include "tcp/TCPServer.h"

#define VERSION 0.4.1

int main(int argc, char** argv) {
	std::string host = "127.0.0.1";
	uint16_t port = 8001;
	uint8_t thread_count = 4;

	try {
		switch (argc) {
			case 4:
				thread_count = (uint8_t) std::stoul(argv[3]);
			case 3:
				port = (uint16_t) std::stoul(argv[2]);
			case 2:
				host = argv[1];

			default:
				break;
		}
	} catch (const std::exception& error) {
		throw std::runtime_error("Arguments parsing error!");
	}

    tcp::TCPServer server(host, port, thread_count);
    server.run();

    return EXIT_SUCCESS;
}