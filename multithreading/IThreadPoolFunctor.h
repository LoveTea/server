//
// Created by kirill on 13.11.17.
//

#ifndef SERVER_ITHREADPOOLFUNCTOR_H
#define SERVER_ITHREADPOOLFUNCTOR_H


#include <deque>

namespace multithreading {
	template <typename QueueParam, typename Worker>
	class ThreadPool;

	template <typename QueueParam, typename Worker>
	class IThreadPoolFunctor {
	protected:
		ThreadPool<QueueParam, Worker>* thread_pool;

	public:
		explicit IThreadPoolFunctor(ThreadPool<QueueParam, Worker>* thread_pool) : thread_pool(thread_pool) {}
		virtual ~IThreadPoolFunctor() = default;

		virtual void operator()() = 0;
	};
}


#endif //SERVER_ITHREADPOOLFUNCTOR_H
