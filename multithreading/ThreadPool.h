//
// Created by kirill on 12.11.17.
//

#ifndef SERVER_THREADPOOL_H
#define SERVER_THREADPOOL_H


#include <mutex>
#include <deque>
#include <thread>
#include <vector>
#include <cassert>
#include <functional>

namespace multithreading {
	template <typename QueueParam, typename Worker>
    class ThreadPool {
		const uint8_t thread_count;
		std::vector<std::thread> threads;

	    std::mutex queue_mutex;
		std::deque<QueueParam> queue;

		bool is_terminate = false;

    public:
	    explicit ThreadPool(uint8_t thread_count);
	    virtual ~ThreadPool();

	    void push_task(QueueParam task);
	    QueueParam pop_task();

		void terminate();
	    bool is_terminated() const;
    };
}

#include "ThreadPool.hpp"


#endif //SERVER_THREADPOOL_H
