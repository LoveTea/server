//
// Created by kirill on 14.11.17.
//

#ifndef SERVER_THREADPOOL_HPP_H
#define SERVER_THREADPOOL_HPP_H


#include "ThreadPool.h"

namespace multithreading {
	template <typename QueueParam, typename Worker>
	ThreadPool<QueueParam, Worker>::ThreadPool(uint8_t thread_count) : thread_count(thread_count) {
		static_assert(std::is_base_of<IThreadPoolFunctor<QueueParam, Worker>, Worker>::value,
			"Worker class must be descendant of IThreadPoolFunctor");

		for (int i = 0; i < thread_count; ++i) {
			threads.emplace_back(std::thread(Worker(this)));
		}
	}

	template <typename QueueParam, typename Worker>
	ThreadPool<QueueParam, Worker>::~ThreadPool() {
		for (std::thread& thread : threads) {
			thread.join();
		}
	}

	template <typename QueueParam, typename Worker>
	void ThreadPool<QueueParam, Worker>::push_task(QueueParam task) {
		std::lock_guard<std::mutex> lock(queue_mutex);
		queue.emplace_front(task);
	}

	template <typename QueueParam, typename Worker>
	QueueParam ThreadPool<QueueParam, Worker>::pop_task() {
		std::unique_lock<std::mutex> lock(queue_mutex, std::try_to_lock);
		if (!lock.owns_lock() || queue.empty()) {
			return (QueueParam) NULL;
		}

		QueueParam task = queue.back();
		queue.pop_back();
		return task;
	}

	template <typename QueueParam, typename Worker>
	void ThreadPool<QueueParam, Worker>::terminate() {
		is_terminate = true;
	}

	template <typename QueueParam, typename Worker>
	bool ThreadPool<QueueParam, Worker>::is_terminated() const {
		return is_terminate;
	}
}


#endif //SERVER_THREADPOOL_HPP_H
