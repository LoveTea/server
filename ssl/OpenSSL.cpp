//
// Created by kirill on 28.11.17.
//

#include <openssl/err.h>

#include "OpenSSL.h"
#include "SSLException.h"

namespace ssl {
	const std::string OpenSSL::KEY_PATH = "certificates/server.key";
	const std::string OpenSSL::CERTIFICATE_PATH = "certificates/server.crt";

	std::vector<std::mutex> OpenSSL::mutexes(CRYPTO_num_locks());

	OpenSSL::OpenSSL() {
		SSL_load_error_strings();
		SSL_library_init();
		init_context();
		threads_init();
	}

	OpenSSL::~OpenSSL() {
		SSL_CTX_free(context);
		ERR_free_strings();
		threads_cleanup();
		EVP_cleanup();
	}

	void OpenSSL::init_context() {
		method = SSLv23_server_method();
		if (!method) {
			throw SSLException(SSL_ERROR_NONE, "SSL failed to create SSL method!");
		}

		context = SSL_CTX_new(method);
		if (!context) {
			throw SSLException(SSL_ERROR_NONE, "SSL failed to initialize SSL context!");
		}

		SSL_CTX_set_ecdh_auto(context, 1);
		SSL_CTX_set_read_ahead(context, 1);

		if (SSL_CTX_use_certificate_file(context, CERTIFICATE_PATH.c_str(), SSL_FILETYPE_PEM) <= 0) {
			throw SSLException(SSL_ERROR_NONE, "SSL failed to load server certificate!");
		}

		if (SSL_CTX_use_PrivateKey_file(context, KEY_PATH.c_str(), SSL_FILETYPE_PEM) <= 0) {
			throw SSLException(SSL_ERROR_NONE, "SSL failed to load server key!");
		}

		if (!SSL_CTX_check_private_key(context)) {
			throw SSLException(SSL_ERROR_NONE, "SSL key doesn't match certificate!");
		}
	}

	void OpenSSL::locking_callback(int mode, int type, const char* file, int line) {
		if (mode & CRYPTO_LOCK) {
			mutexes[type].lock();
		} else {
			mutexes[type].unlock();
		}
	}

	void OpenSSL::thread_id_callback(CRYPTO_THREADID* thread_id) {
		CRYPTO_THREADID_set_numeric(thread_id, pthread_self());
	}

	void OpenSSL::threads_init() {
		CRYPTO_THREADID_set_callback(&OpenSSL::thread_id_callback);
		CRYPTO_set_locking_callback(&OpenSSL::locking_callback);
	}

	void OpenSSL::threads_cleanup() {
		CRYPTO_THREADID_set_callback(nullptr);
		CRYPTO_set_locking_callback(nullptr);
	}

	OpenSSL& OpenSSL::get() {
		static OpenSSL ssl;
		return ssl;
	}

	std::unique_ptr<SSL, void(*)(SSL*)> OpenSSL::get_ssl_socket(int socket) {
		std::unique_ptr<SSL, void(*)(SSL*)> pointer(SSL_new(context), [](SSL* ssl) { SSL_free(ssl); });
		if (!pointer || !SSL_set_fd(pointer.get(), socket)) {
			throw SSLException(SSL_ERROR_NONE, "SSL socket creation failed!");
		}
		return pointer;
	}

	void OpenSSL::handshake(SSL* ssl) {
		int rc = SSL_accept(ssl);
		if (rc <= 0) {
			int error = SSL_get_error(ssl, rc);
			throw SSLException(error, "SSL handshake error!");
		}
	}

	int OpenSSL::read(SSL* ssl, void* buffer, uint32_t size) {
		int read_count = SSL_read(ssl, buffer, size);
		if (read_count < 0) {
			int error = SSL_get_error(ssl, read_count);
			throw SSLException(error, "SSL socket read failed! ");
		}

		return read_count;
	}

	int OpenSSL::write(SSL* ssl, const void* buffer, uint32_t size) {
		int write_count = SSL_write(ssl, buffer, size);
		if (write_count <= 0) {
			int error = SSL_get_error(ssl, write_count);
			throw SSLException(error, "SSL socket write failed!");
		}

		return write_count;
	}
}
