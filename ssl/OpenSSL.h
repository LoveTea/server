//
// Created by kirill on 28.11.17.
//

#ifndef SERVER_SSLSINGLETON_H
#define SERVER_SSLSINGLETON_H

#include <mutex>
#include <string>
#include <memory>
#include <vector>
#include <pthread.h>
#include <openssl/ssl.h>


namespace ssl {
	class OpenSSL final {
		static const std::string KEY_PATH;
		static const std::string CERTIFICATE_PATH;

		SSL_CTX* context;
		const SSL_METHOD* method;

		static std::vector<std::mutex> mutexes;

		OpenSSL();
		~OpenSSL();

		void init_context();

		static void locking_callback(int mode, int type, const char* file, int line);
		static void thread_id_callback(CRYPTO_THREADID* thread_id);
		void threads_init();
		void threads_cleanup();

	public:
		OpenSSL(const OpenSSL&) = delete;
		OpenSSL& operator=(const OpenSSL&) = delete;

		static OpenSSL& get();

		std::unique_ptr<SSL, void(*)(SSL*)> get_ssl_socket(int socket);
		void handshake(SSL* ssl);
		int read(SSL* ssl, void* buffer, uint32_t size);
		int write(SSL* ssl, const void* buffer, uint32_t size);
	};
}


#endif //SERVER_SSLSINGLETON_H
