//
// Created by kirill on 28.11.17.
//

#ifndef SERVER_SSLEXCEPTION_H
#define SERVER_SSLEXCEPTION_H

#include "../utils/BaseException.h"

namespace ssl {
	class SSLException final : public utils::BaseException {
	public:
		const int code;

		explicit SSLException(int error_code, const std::string& message)
			: code(error_code), BaseException(message) {}
	};
}

#endif //SERVER_SSLEXCEPTION_H
