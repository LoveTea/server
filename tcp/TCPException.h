//
// Created by kirill on 11.11.17.
//

#ifndef SERVER_TCPEXCEPTION_H
#define SERVER_TCPEXCEPTION_H


namespace tcp {
    class TCPException : public utils::BaseException {
    public:
        explicit TCPException(const std::string& message) : BaseException(message) {}
    };
}


#endif //SERVER_TCPEXCEPTION_H
