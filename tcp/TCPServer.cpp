//
// Created by kirill on 11.11.17.
//

#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>

#include "TCPServer.h"
#include "TCPException.h"

namespace tcp {
    TCPServer::TCPServer(const std::string& ip, uint16_t port, uint8_t thread_count)
		: ip(ip), port(port), thread_pool(thread_count) {
        std::memset(&address, 0, sizeof(sockaddr_in));
        address.sin_family = AF_INET;
        address.sin_port = htons(port);
        if (inet_pton(AF_INET, ip.c_str(), &address.sin_addr) != 1) {
            throw TCPException("IP address set failed!");
        }

        main_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (main_socket == -1) {
            throw TCPException("Socket init failed! " + std::string(std::strerror(errno)));
        }

	    int option_value = 1;
	    if (setsockopt(main_socket, SOL_SOCKET, SO_REUSEADDR, &option_value, sizeof(option_value))) {
		    close(main_socket);
		    throw TCPException("Socket option set failed! " + std::string(std::strerror(errno)));
	    }
    }

    TCPServer::~TCPServer() {
        close(main_socket);
		thread_pool.terminate();
    }

    void TCPServer::run() {
        if (bind(main_socket, (sockaddr*) &address, sizeof(address))) {
            throw TCPException("Can't bind socket! " + std::string(std::strerror(errno)));
        }

        if (listen(main_socket, 10)) {
            throw TCPException("Socket listen failed! " + std::string(std::strerror(errno)));
        }

	    while (true) {
		    int socket = accept(main_socket, nullptr, nullptr);
			thread_pool.push_task(socket);
	    }
    }
}