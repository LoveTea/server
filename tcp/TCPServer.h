//
// Created by kirill on 11.11.17.
//

#ifndef SERVER_TCPSERVER_H
#define SERVER_TCPSERVER_H


#include <string>
#include <netinet/in.h>

#ifdef EPOLL_SERVER
#include "connection/EpollConnectionHandler.h"
#elif EVENT_SERVER
#include "connection/EventConnectionHandler.h"
#else
#include "connection/SelectConnectionHandler.h"
#endif

#include "connection/Connection.h"
#include "../multithreading/ThreadPool.h"

namespace tcp {
    class TCPServer {
        const std::string ip;
        uint16_t port;
        int main_socket;
        sockaddr_in address;

		multithreading::ThreadPool<int,
#ifdef EPOLL_SERVER
        connection::EpollConnectionHandler
#elif EVENT_SERVER
		connection::EventConnectionHandler
#else
		connection::SelectConnectionHandler
#endif
		> thread_pool;

    public:
        TCPServer(const std::string& ip, uint16_t port, uint8_t thread_count);
        virtual ~TCPServer();

        void run();
    };
}


#endif //SERVER_TCPSERVER_H
