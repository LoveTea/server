//
// Created by kirill on 13.11.17.
//

#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>

#include "Connection.h"
#include "ConnectionException.h"

namespace tcp {
namespace connection {
	Connection::Connection(int socket) : IConnection(socket) {}

	uint32_t Connection::read_socket() {
		read_buffer.reserve(read_buffer.size() + READ_MAX);
		int64_t read_count = recv(socket, &read_buffer[read_buffer.size()], READ_MAX, 0);
		if (read_count == -1) {
			socket_error = errno;
			throw ConnectionException("Socket read failed! " + std::string(std::strerror(errno)));
		}
		if (read_count == 0) {
			closed_by_client = true;
		} else {
			update_timeout();
		}
		read_buffer.set_size(read_buffer.size() + read_count);

		return (uint32_t) read_count;
	}

	uint32_t Connection::write_socket() {
		int64_t write_count = write(socket, write_buffer.data(), write_buffer.size());
		if (write_count == -1) {
			socket_error = errno;
			throw ConnectionException("Socket write failed! " + std::string(std::strerror(errno)));
		}

		if (write_count > 0) {
			update_timeout();
			processed(write_buffer, (uint32_t) write_count);
		}

		return (uint32_t) write_count;
	}

	const utils::Buffer<char>& Connection::get_read_buffer() const {
		return read_buffer;
	}

	void Connection::read_processed(uint64_t size) {
		processed(read_buffer, size);
	}

	void Connection::set_write_buffer(const char* cbegin, const char* cend) {
		write_buffer.append(cbegin, cend);
	}

	bool Connection::is_write_ready() const {
		return !write_buffer.empty();
	}
}
}