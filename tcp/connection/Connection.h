//
// Created by kirill on 13.11.17.
//

#ifndef SERVER_CONNECTION_H
#define SERVER_CONNECTION_H


#include <vector>
#include <chrono>
#include <cstdint>

#include "IConnection.h"
#include "../../utils/Buffer.h"

namespace tcp {
namespace connection {
	class Connection : public IConnection {
	protected:
		utils::Buffer<char> read_buffer, write_buffer;

	public:
		explicit Connection(int socket);

		uint32_t read_socket() override;
		uint32_t write_socket() override;

		const utils::Buffer<char>& get_read_buffer() const override;
		void read_processed(uint64_t size) override;

		void set_write_buffer(const char* cbegin, const char* cend) override;
		bool is_write_ready() const override;
	};
}
}


#endif //SERVER_CONNECTION_H
