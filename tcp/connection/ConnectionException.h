//
// Created by kirill on 16.11.17.
//

#ifndef SERVER_CONNECTIONEXCEPTION_H
#define SERVER_CONNECTIONEXCEPTION_H

#include "../../utils/BaseException.h"

namespace tcp {
namespace connection {
	class ConnectionException final : public utils::BaseException {
	public:
		explicit ConnectionException(const std::string& message) : BaseException(message) {}
	};
}
}


#endif //SERVER_CONNECTIONEXCEPTION_H
