//
// Created by kirill on 25.11.17.
//

#include <mutex>
#include <thread>
#include <unistd.h>
#include <sys/epoll.h>

#include "../../http/HTTP.h"
#include "ConnectionException.h"
#include "EpollConnectionHandler.h"
#include "../../multithreading/ThreadPool.h"

#ifdef SSL_SERVER
#include "SSLConnection.h"
#else
#include "Connection.h"
#endif

namespace tcp {
namespace connection {
	EpollConnectionHandler::EpollConnectionHandler(
		multithreading::ThreadPool<int, EpollConnectionHandler>* thread_pool) : IThreadPoolFunctor(thread_pool) {}

	EpollConnectionHandler::~EpollConnectionHandler() {
		if (epoll_descriptor) {
			close(epoll_descriptor);
		}
	}

	void EpollConnectionHandler::init_epoll() {
		epoll_descriptor = epoll_create(1);
		if (epoll_descriptor == -1) {
			throw ConnectionException("Epoll create failed! " + std::string(std::strerror(errno)));
		}
	}

	void EpollConnectionHandler::add_listen(EpollConnection& epoll_connection) {
		epoll_event event { EPOLLIN | EPOLLOUT | EPOLLET, { .ptr = &epoll_connection } };
		if (epoll_ctl(epoll_descriptor, EPOLL_CTL_ADD, epoll_connection.connection->get_socket(), &event)) {
			throw ConnectionException("Epoll add socket error! " + std::string(std::strerror(errno)));
		}
	}

	void EpollConnectionHandler::remove_listen(const EpollConnection& epoll_connection) {
		if (epoll_ctl(epoll_descriptor, EPOLL_CTL_DEL, epoll_connection.connection->get_socket(), nullptr)) {
			throw ConnectionException("Epoll remove socket error! " + std::string(std::strerror(errno)));
		}
	}

	void EpollConnectionHandler::handle_connections() {
		int socket = thread_pool->pop_task();
		if (socket) {
			connections.emplace_back(EpollConnection{ 0,
#ifdef SSL_SERVER
				std::make_shared<SSLConnection>(socket)
#else
				std::make_shared<Connection>(socket)
#endif
			});
			add_listen(connections.back());
		}

		if (!connections.empty()) {
			epoll_event events[connections.size()];
			int event_count = epoll_wait(epoll_descriptor, events, connections.size(), 0);
			if (event_count == -1) {
				throw ConnectionException("Epoll wait failed! " + std::string(std::strerror(errno)));
			}

			for (int i = 0; i < event_count; ++i) {
				EpollConnection& epoll_connection = *((EpollConnection*) events[i].data.ptr);
				epoll_connection.events = events[i].events;
			}

			auto iterator = connections.begin();
			while (iterator != connections.end()) {
				if (iterator->connection->is_connection_timeout() || iterator->connection->get_socket_error() != 0
					|| iterator->connection->is_waiting_for_close() && !iterator->connection->is_write_ready()
					|| iterator->connection->is_closed_by_client() || iterator->events & EPOLLERR)
				{
					remove_listen(*iterator);
					iterator = connections.erase(iterator);
					continue;
				}

				if (iterator->events & EPOLLOUT && iterator->connection->is_write_ready()) {
					try {
						iterator->connection->write_socket();
					} catch (const ConnectionException& error) {
						if (iterator->connection->get_socket_error() == EAGAIN) {
							iterator->events &= ~EPOLLOUT;
							iterator->connection->clear_socket_error();
						} else {
							throw;
						}
					}
				} else {
					if (iterator->events & EPOLLIN) {
						try {
							handle_read(iterator->connection);
						} catch (const ConnectionException& error) {
							if (iterator->connection->get_socket_error() == EAGAIN) {
								iterator->events &= ~EPOLLIN;
								iterator->connection->clear_socket_error();
							} else {
								throw;
							}
						}
					}
				}

				++iterator;
			}
		}
	}

	void EpollConnectionHandler::handle_read(std::shared_ptr<IConnection>& connection) {
		if (connection->read_socket() != 0) {
			const utils::Buffer<char>& read_buffer = connection->get_read_buffer();
			http::HTTPRequest request = http::HTTP::parse_request(read_buffer.cbegin(), read_buffer.cend());
			http::HTTPResponse response = http::HTTP::resolve(request);
			if (response.is_connection_close()) {
				connection->set_waiting_for_close();
			}
			std::string raw_response = response.to_string();
			connection->set_write_buffer(raw_response.data(), raw_response.data() + raw_response.size());
			connection->read_processed(request.get_request_size());
		}
	}

	void EpollConnectionHandler::operator()() {
		init_epoll();

		while (true) {
			if (thread_pool->is_terminated()) {
				return;
			}

			try {
				handle_connections();
			} catch (const std::exception& error) {
				printf("WORKER EXCEPTION: %s\n", error.what());
			}

			if (connections.empty()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
			}
		}
	}
}
}