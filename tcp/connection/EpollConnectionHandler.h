//
// Created by kirill on 25.11.17.
//

#ifndef SERVER_EPOLLCONNECTIONHANDLER_H
#define SERVER_EPOLLCONNECTIONHANDLER_H


#include <list>

#include "IConnection.h"
#include "../../multithreading/IThreadPoolFunctor.h"

namespace tcp {
namespace connection {
	class EpollConnectionHandler
		: public multithreading::IThreadPoolFunctor<int, EpollConnectionHandler>
	{
		struct EpollConnection {
			uint32_t events;
			std::shared_ptr<IConnection> connection;
		};

		std::list<EpollConnection> connections;
		int epoll_descriptor = 0;

		void init_epoll();
		void add_listen(EpollConnection& epoll_connection);
		void remove_listen(const EpollConnection& epoll_connection);

		void handle_connections();
		void handle_read(std::shared_ptr<IConnection>& connection);

	public:
		explicit EpollConnectionHandler(
			multithreading::ThreadPool<int, EpollConnectionHandler>* thread_pool);
		~EpollConnectionHandler() override;

		void operator()() override;
	};
}
}


#endif //SERVER_EPOLLCONNECTIONHANDLER_H
