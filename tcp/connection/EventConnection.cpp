//
// Created by kirill on 23.12.17.
//

#include <event2/buffer.h>

#include "EventConnection.h"
#include "ConnectionException.h"

namespace tcp {
namespace connection {
	EventConnection::EventConnection(int socket, event_base* base_event)
		: IConnection(socket),
		event_buffer(nullptr, [](bufferevent* buffer) { bufferevent_free(buffer); })
	{
		event_buffer.reset(bufferevent_socket_new(base_event, socket, 0));
		if (!event_buffer) {
			throw ConnectionException("Connection init failed!");
		}

		bufferevent_setcb(event_buffer.get(), &EventConnection::read_callback, nullptr, nullptr, this);

		if (bufferevent_enable(event_buffer.get(), EV_READ | EV_WRITE)) {
			throw ConnectionException("Connection adjustment failed!");
		}
	}

	void EventConnection::read_callback(bufferevent* event_buffer, void* arg) {
		EventConnection* connection = static_cast<EventConnection*>(arg);
		utils::Buffer<char>& read_buffer = connection->read_buffer;
		evbuffer* event_read_buffer = bufferevent_get_input(event_buffer);
		size_t data_length = evbuffer_get_length(event_read_buffer);
		read_buffer.reserve(read_buffer.size() + (data_length > READ_MAX ? data_length : READ_MAX));

		size_t data_read = bufferevent_read(event_buffer, read_buffer.begin() + read_buffer.size(), data_length);
		read_buffer.set_size(read_buffer.size() + data_read);
		connection->last_read = data_read;
	}

	uint32_t EventConnection::read_socket() {
		uint32_t temp = last_read;
		last_read = 0;
		return temp;
	}

	uint32_t EventConnection::write_socket() {
		return 0;
	}

	const utils::Buffer<char>& EventConnection::get_read_buffer() const {
		return read_buffer;
	}

	void EventConnection::read_processed(uint64_t size) {
		processed(read_buffer, size);
	}

	void EventConnection::set_write_buffer(const char* cbegin, const char* cend) {
		if (bufferevent_write(event_buffer.get(), cbegin, cend - cbegin)) {
			throw ConnectionException("Write buffer set failed!");
		}
	}

	bool EventConnection::is_write_ready() const {
		return false;
	}
}
}