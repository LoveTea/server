//
// Created by kirill on 23.12.17.
//

#ifndef SERVER_EVENTCONNECTION_H
#define SERVER_EVENTCONNECTION_H


#include <memory>
#include <event2/event.h>
#include <event2/bufferevent.h>

#include "IConnection.h"

namespace tcp {
namespace connection {
	class EventConnection : public IConnection {
	protected:
		uint32_t last_read = 0;
		utils::Buffer<char> read_buffer;
		std::unique_ptr<bufferevent, void(*)(bufferevent*)> event_buffer;

		static void read_callback(bufferevent* event_buffer, void* arg);

	public:
		explicit EventConnection(int socket, event_base* base_event);

		uint32_t read_socket() override;
		uint32_t write_socket() override;

		const utils::Buffer<char>& get_read_buffer() const override;
		void read_processed(uint64_t size) override;

		void set_write_buffer(const char* cbegin, const char* cend) override;
		bool is_write_ready() const override;
	};
}
}


#endif //SERVER_EVENTCONNECTION_H
