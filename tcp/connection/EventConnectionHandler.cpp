//
// Created by kirill on 23.12.17.
//

#include <chrono>
#include <thread>

#include "../../http/HTTP.h"
#include "ConnectionException.h"
#include "EventConnectionHandler.h"
#include "../../multithreading/ThreadPool.h"

#ifdef SSL_SERVER
#include "SSLEventConnection.h"
#else
#include "EventConnection.h"
#endif

namespace tcp {
namespace connection {
	EventConnectionHandler::EventConnectionHandler(
		multithreading::ThreadPool<int, EventConnectionHandler>* thread_pool)
		: IThreadPoolFunctor(thread_pool), main_event(nullptr, [](event_base* event) { event_base_free(event); }) {}

	void EventConnectionHandler::init_event() {
		event_config* config = event_config_new();
		if (!config) {
			throw ConnectionException("Event init failed! Event config not allocated!");
		}

		event_config_require_features(config, EV_FEATURE_ET);
		event_config_set_flag(config, EVENT_BASE_FLAG_NOLOCK);

		main_event.reset(event_base_new_with_config(config));
		if (!main_event) {
			throw ConnectionException("Event init failed!");
		}

		event_config_free(config);
	}

	void EventConnectionHandler::handle_connections() {
		int socket = thread_pool->pop_task();
		if (socket) {
			connections.emplace_back(
#ifdef SSL_SERVER
				new SSLEventConnection(socket, main_event.get())
#else
				new EventConnection(socket, main_event.get())
#endif
			);
		}

		event_base_loop(main_event.get(), EVLOOP_NONBLOCK);

		if (!connections.empty()) {
			auto iterator = connections.begin();
			while (iterator != connections.end()) {
				if ((*iterator)->is_connection_timeout() || (*iterator)->get_socket_error() != 0
					|| (*iterator)->is_waiting_for_close() && !(*iterator)->is_write_ready()
					|| (*iterator)->is_closed_by_client())
				{
					iterator = connections.erase(iterator);
					continue;
				}

				if ((*iterator)->read_socket()) {
					const utils::Buffer<char>& read_buffer = (*iterator)->get_read_buffer();
					http::HTTPRequest request = http::HTTP::parse_request(read_buffer.cbegin(), read_buffer.cend());
					http::HTTPResponse response = http::HTTP::resolve(request);
					if (response.is_connection_close()) {
						(*iterator)->set_waiting_for_close();
					}
					std::string raw_response = response.to_string();
					(*iterator)->set_write_buffer(raw_response.data(), raw_response.data() + raw_response.size());
					(*iterator)->read_processed(request.get_request_size());
				}
				
				++iterator;
			}	
		}
	}

	void EventConnectionHandler::operator()() {
		init_event();

		while (true) {
			if (thread_pool->is_terminated()) {
				return;
			}

			try {
				handle_connections();
			} catch (const std::exception& error) {
				printf("WORKER EXCEPTION: %s\n", error.what());
			}

			if (connections.empty()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
			}
		}
	}
}
}