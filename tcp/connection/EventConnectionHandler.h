//
// Created by kirill on 23.12.17.
//

#ifndef SERVER_EVENTCONNECTIONHANDLER_H
#define SERVER_EVENTCONNECTIONHANDLER_H

#include <list>
#include <memory>
#include <event2/event.h>

#include "IConnection.h"
#include "../../multithreading/IThreadPoolFunctor.h"

namespace tcp {
namespace connection {
	class EventConnectionHandler
		: public multithreading::IThreadPoolFunctor<int, EventConnectionHandler>
	{
		std::list<std::unique_ptr<IConnection>> connections;
		std::unique_ptr<event_base, void(*)(event_base*)> main_event;

		void init_event();
		void handle_connections();

	public:
		explicit EventConnectionHandler(
			multithreading::ThreadPool<int, EventConnectionHandler>* thread_pool);

		void operator()() override;
	};
}
}


#endif //SERVER_EVENTCONNECTIONHANDLER_H
