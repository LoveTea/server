//
// Created by kirill on 23.12.17.
//

#include <fcntl.h>
#include <zconf.h>

#include "IConnection.h"
#include "ConnectionException.h"

namespace tcp {
namespace connection {
	const uint16_t IConnection::READ_MAX = 1024;
	const uint32_t IConnection::KEEP_ALIVE_TIMEOUT = 5000;

	IConnection::IConnection(int socket) : socket(socket) {
		make_non_block();
		update_timeout();
	}

	IConnection::~IConnection() {
		close(socket);
	}

	void IConnection::make_non_block() {
		int flags = fcntl(socket, F_GETFL);
		if (flags == -1) {
			throw ConnectionException("Get socket flags failed! " + std::string(std::strerror(errno)));
		}
		if (fcntl(socket, F_SETFL, flags | O_NONBLOCK)) {
			throw ConnectionException("Set socket to non blocking mode failed! " + std::string(std::strerror(errno)));
		}
	}

	void IConnection::update_timeout() {
		connection_timeout = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch());
	}

	void IConnection::processed(utils::Buffer<char>& buffer, uint64_t size) {
		utils::Buffer<char> temp(buffer.cbegin() + size, buffer.cend());
		buffer.swap(temp);
	}

	int IConnection::get_socket() {
		return socket;
	}

	int IConnection::get_socket_error() const {
		return socket_error;
	}

	void IConnection::clear_socket_error() {
		socket_error = 0;
	}

	bool IConnection::is_closed_by_client() const {
		return closed_by_client;
	}

	bool IConnection::is_connection_timeout() const {
		auto now = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch());

		return (now - connection_timeout).count() > KEEP_ALIVE_TIMEOUT;
	}

	void IConnection::set_waiting_for_close() {
		waiting_for_close = true;
	}

	bool IConnection::is_waiting_for_close() const {
		return waiting_for_close;
	}
}
}