//
// Created by kirill on 23.12.17.
//

#ifndef SERVER_ICONNECTION_H
#define SERVER_ICONNECTION_H

#include <chrono>

#include "../../utils/Buffer.h"

namespace tcp {
namespace connection {
	class IConnection {
	public:
		static const uint16_t READ_MAX;
		static const uint32_t KEEP_ALIVE_TIMEOUT;

	protected:
		const int socket;
		int socket_error = 0;
		bool closed_by_client = false;
		bool waiting_for_close = false;
		std::chrono::milliseconds connection_timeout;

		virtual void make_non_block();
		virtual void update_timeout();
		virtual void processed(utils::Buffer<char>& buffer, uint64_t size);

	public:
		explicit IConnection(int socket);
		IConnection(const IConnection&) = delete;
		IConnection& operator=(const IConnection&) = delete;
		virtual ~IConnection();

		virtual int get_socket();
		virtual int get_socket_error() const;
		virtual void clear_socket_error();
		virtual bool is_closed_by_client() const;
		virtual bool is_connection_timeout() const;
		virtual void set_waiting_for_close();
		virtual bool is_waiting_for_close() const;
		virtual uint32_t read_socket() = 0;
		virtual uint32_t write_socket() = 0;

		virtual const utils::Buffer<char>& get_read_buffer() const = 0;
		virtual void read_processed(uint64_t size) = 0;

		virtual void set_write_buffer(const char* cbegin, const char* cend) = 0;
		virtual bool is_write_ready() const = 0;
	};
}
}

#endif //SERVER_ICONNECTION_H
