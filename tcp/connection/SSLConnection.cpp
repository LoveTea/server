//
// Created by kirill on 28.11.17.
//

#include "SSLConnection.h"
#include "ConnectionException.h"
#include "../../ssl/SSLException.h"

namespace tcp {
namespace connection {
	SSLConnection::SSLConnection(int socket)
		: Connection(socket), openssl(ssl::OpenSSL::get()), ssl(openssl.get_ssl_socket(socket)) {}

	void SSLConnection::handshake() {
		try {
			openssl.handshake(ssl.get());
		} catch (const ssl::SSLException& error) {
			if (error.code == SSL_ERROR_WANT_READ || error.code == SSL_ERROR_WANT_WRITE) {
				socket_error = EAGAIN;
				throw ConnectionException("Repeat!");
			}
			socket_error = EIO;
			throw;
		}

		handshake_done = true;
	}

	uint32_t SSLConnection::read_socket() {
		if (!handshake_done) {
			handshake();
			return 0;
		}

		read_buffer.reserve(read_buffer.size() + READ_MAX);
		int read_count = 0;
		try {
			read_count = openssl.read(ssl.get(), read_buffer.data(), READ_MAX);
		} catch (const ssl::SSLException& error) {
			if (error.code == SSL_ERROR_WANT_READ || error.code == SSL_ERROR_WANT_WRITE) {
				socket_error = EAGAIN;
				throw ConnectionException("Repeat!");
			}
			socket_error = EIO;
			throw;
		}

		if (read_count == 0) {
			closed_by_client = true;
		} else {
			update_timeout();
		}
		read_buffer.set_size(read_buffer.size() + read_count);

		return (uint32_t) read_count;
	}

	uint32_t SSLConnection::write_socket() {
		if (!handshake_done) {
			handshake();
			return 0;
		}

		int write_count = 0;
		try {
			write_count = openssl.write(ssl.get(), write_buffer.data(), write_buffer.size());
		} catch (const ssl::SSLException& error) {
			if (error.code == SSL_ERROR_WANT_READ || error.code == SSL_ERROR_WANT_WRITE) {
				socket_error = EAGAIN;
				throw ConnectionException("Repeat!");
			}
			socket_error = EIO;
			throw;
		}

		if (write_count > 0) {
			update_timeout();
			processed(write_buffer, (uint32_t) write_count);
		}

		return (uint32_t) write_count;
	}
}
}