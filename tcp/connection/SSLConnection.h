//
// Created by kirill on 28.11.17.
//

#ifndef SERVER_SSLCONNECTION_H
#define SERVER_SSLCONNECTION_H


#include <openssl/ossl_typ.h>

#include "Connection.h"
#include "../../ssl/OpenSSL.h"

namespace tcp {
namespace connection {
	class SSLConnection : public Connection {
		ssl::OpenSSL& openssl;
		std::unique_ptr<SSL, void(*)(SSL*)> ssl;
		bool handshake_done = false;

		void handshake();

	public:
		explicit SSLConnection(int socket);

		uint32_t read_socket() override;
		uint32_t write_socket() override;
	};
}
}


#endif //SERVER_SSLCONNECTION_H
