//
// Created by kirill on 25.12.17.
//

#include <event2/bufferevent_ssl.h>

#include "SSLEventConnection.h"
#include "ConnectionException.h"

namespace tcp {
namespace connection {
	SSLEventConnection::SSLEventConnection(int socket, event_base* base_event)
		: EventConnection(socket, base_event),
		  openssl(ssl::OpenSSL::get()),
		  ssl(openssl.get_ssl_socket(socket))
	{
		event_buffer.reset(bufferevent_openssl_socket_new(base_event, socket, ssl.get(), BUFFEREVENT_SSL_ACCEPTING, 0));
		if (!event_buffer) {
			throw ConnectionException("Connection init failed!");
		}

		bufferevent_setcb(event_buffer.get(), &EventConnection::read_callback, nullptr, nullptr, this);

		if (bufferevent_enable(event_buffer.get(), EV_READ | EV_WRITE)) {
			throw ConnectionException("Connection adjustment failed!");
		}
	}
}
}