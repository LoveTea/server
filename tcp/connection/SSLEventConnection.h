//
// Created by kirill on 25.12.17.
//

#ifndef SERVER_SSLEVENTCONNECTION_H
#define SERVER_SSLEVENTCONNECTION_H


#include <openssl/ossl_typ.h>

#include "EventConnection.h"
#include "../../ssl/OpenSSL.h"

namespace tcp {
namespace connection {
	class SSLEventConnection : public EventConnection {
		ssl::OpenSSL& openssl;
		std::unique_ptr<SSL, void(*)(SSL*)> ssl;

	public:
		SSLEventConnection(int socket, event_base* base_event);
	};
}
}


#endif //SERVER_SSLEVENTCONNECTION_H
