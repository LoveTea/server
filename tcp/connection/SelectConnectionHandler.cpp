//
// Created by kirill on 13.11.17.
//

#include <cstring>
#include <fcntl.h>

#include "../../http/HTTP.h"
#include "ConnectionException.h"
#include "SelectConnectionHandler.h"
#include "../../multithreading/ThreadPool.h"

#ifdef SSL_SERVER
#include "SSLConnection.h"
#else
#include "Connection.h"
#endif

namespace tcp {
namespace connection {
	SelectConnectionHandler::SelectConnectionHandler(
		multithreading::ThreadPool<int, SelectConnectionHandler>* thread_pool) : IThreadPoolFunctor(thread_pool) {}

	void SelectConnectionHandler::handle_connections() {
		int socket = thread_pool->pop_task();
		if (socket) {
			connections.emplace_back(
#ifdef SSL_SERVER
				new SSLConnection(socket)
#else
				new Connection(socket)
#endif
			);
		}

		if (!connections.empty()) {
			auto current_iterator = connections.begin();
			while (current_iterator != connections.end()) {
				if ((*current_iterator)->is_connection_timeout() || (*current_iterator)->get_socket_error() != 0
					|| (*current_iterator)->is_waiting_for_close() && !(*current_iterator)->is_write_ready()
					|| (*current_iterator)->is_closed_by_client())
				{
					current_iterator = connections.erase(current_iterator);
					continue;
				}

				int max_socket = 0;
				auto const start_iterator = current_iterator;
				uint64_t read_set_size = 0, write_set_size = 0;

				FD_ZERO(&read_set);
				FD_ZERO(&write_set);

				while (current_iterator != connections.end()) {
					if (read_set_size == FD_SETSIZE || write_set_size == FD_SETSIZE) {
						break;
					}

					int socket = (*current_iterator)->get_socket();
					if ((*current_iterator)->is_write_ready()) {
						FD_SET(socket, &write_set);
						++write_set_size;
					} else {
						FD_SET(socket, &read_set);
						++read_set_size;
					}

					max_socket = max_socket > socket ? max_socket : socket;

					++current_iterator;
				}

				timeval timeout{ 0, 0 };
				int sockets_ready = select(max_socket + 1, &read_set, &write_set, nullptr, &timeout);
				if (sockets_ready == -1) {
					throw ConnectionException("Select failed! " + std::string(std::strerror(errno)));
				}

				if (sockets_ready > 0) {
					auto end_iterator = current_iterator;
					current_iterator = start_iterator;
					while (current_iterator != end_iterator) {
						int socket = (*current_iterator)->get_socket();
						if (FD_ISSET(socket, &read_set)) {
							try {
								handle_read(*current_iterator);
							} catch (const ConnectionException& error) {
								if ((*current_iterator)->get_socket_error() != EAGAIN) {
									throw;
								}
								(*current_iterator)->clear_socket_error();
							}
						} else {
							if (FD_ISSET(socket, &write_set)) {
								try {
									(*current_iterator)->write_socket();
								} catch (const ConnectionException& error) {
									if ((*current_iterator)->get_socket_error() != EAGAIN) {
										throw;
									}
									(*current_iterator)->clear_socket_error();
								}
							}
						}

						++current_iterator;
					}
				}
			}
		}
	}

	void SelectConnectionHandler::handle_read(std::unique_ptr<IConnection>& connection) {
		if (connection->read_socket() != 0) {
			const utils::Buffer<char>& read_buffer = connection->get_read_buffer();
			http::HTTPRequest request = http::HTTP::parse_request(read_buffer.cbegin(), read_buffer.cend());
			http::HTTPResponse response = http::HTTP::resolve(request);
			if (response.is_connection_close()) {
				connection->set_waiting_for_close();
			}
			std::string raw_response = response.to_string();
			connection->set_write_buffer(raw_response.data(), raw_response.data() + raw_response.size());
			connection->read_processed(request.get_request_size());
		}
	}

	void SelectConnectionHandler::operator()() {
		while (true) {
			if (thread_pool->is_terminated()) {
				return;
			}

			try {
				handle_connections();
			} catch (const std::exception& error) {
				printf("WORKER EXCEPTION: %s\n", error.what());
			}

			if (connections.empty()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
			}
		}
	}
}
}