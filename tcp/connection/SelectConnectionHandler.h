//
// Created by kirill on 13.11.17.
//

#ifndef SERVER_CONNECTIONHANDLER_H
#define SERVER_CONNECTIONHANDLER_H


#include <list>
#include <mutex>
#include <memory>

#include "IConnection.h"
#include "../../multithreading/IThreadPoolFunctor.h"

namespace tcp {
namespace connection {
	class SelectConnectionHandler
		: public multithreading::IThreadPoolFunctor<int, SelectConnectionHandler> {
		std::list<std::unique_ptr<IConnection>> connections;
		fd_set read_set, write_set;

		void handle_connections();
		void handle_read(std::unique_ptr<IConnection>& connection);

	public:
		explicit SelectConnectionHandler(
			multithreading::ThreadPool<int, SelectConnectionHandler>* thread_pool);

		void operator()() override;
	};
}
}


#endif //SERVER_CONNECTIONHANDLER_H
