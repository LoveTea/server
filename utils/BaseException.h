//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_BASEEXCEPTION_H
#define SERVER_BASEEXCEPTION_H


#include <string>
#include <exception>

namespace utils {
	class BaseException : public std::exception {
		const std::string message;

	public:
		explicit BaseException(const std::string& message) : message(message) {}

		const char* what() const noexcept override {
			return message.c_str();
		}
	};
}


#endif //SERVER_BASEEXCEPTION_H
