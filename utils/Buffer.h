//
// Created by kirill on 16.11.17.
//

#ifndef SERVER_BUFFER_H
#define SERVER_BUFFER_H


#include <memory>

namespace utils {
	template <typename ElementType>
	class Buffer {
		uint64_t element_size = 0, actual_size = 0;

		std::unique_ptr<ElementType[]> buffer;

		void allocate(uint64_t size);

	public:
		Buffer() = default;
		explicit Buffer(uint64_t size);
		Buffer(const ElementType* cbegin, const ElementType* cend);
		Buffer(Buffer<ElementType>&& move_buffer) noexcept;

		ElementType& operator[](int index);

		ElementType* data();
		void reserve(uint64_t size);
		uint64_t size() const;
		void set_size(uint64_t size);
		bool empty() const;

		ElementType* begin();
		const ElementType* cbegin() const;
		ElementType* end();
		const ElementType* cend() const;
		void append(const ElementType* cbegin, const ElementType* cend);

		void swap(Buffer<ElementType>& swap_buffer);
	};
}

#include "Buffer.hpp"


#endif //SERVER_BUFFER_H
