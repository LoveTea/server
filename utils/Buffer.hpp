//
// Created by kirill on 16.11.17.
//

#ifndef SERVER_BUFFER_HPP
#define SERVER_BUFFER_HPP


#include <cstring>

#include "Buffer.h"
#include "BufferException.h"

namespace utils {
	template <typename ElementType>
	Buffer<ElementType>::Buffer(uint64_t size) {
		allocate(size);
	}

	template <typename ElementType>
	ElementType& Buffer<ElementType>::operator[](int index) {
		return buffer[index];
	}

	template <typename ElementType>
	void Buffer<ElementType>::allocate(uint64_t size) {
		ElementType* temp = new ElementType[size];

		uint64_t new_element_size = element_size > size ? size : element_size;
		if (element_size > 0) {
			std::memcpy(temp, buffer.get(), element_size > size ? size : element_size);
		}

		buffer.reset(temp);
		actual_size = size;
		element_size = new_element_size;
	}

	template <typename ElementType>
	Buffer<ElementType>::Buffer(const ElementType* cbegin, const ElementType* cend) {
		uint64_t distance = cend - cbegin;
		allocate(distance);
		std::memcpy(buffer.get(), cbegin, distance);
		element_size = distance;
	}

	template <typename ElementType>
	Buffer<ElementType>::Buffer(Buffer<ElementType>&& move_buffer) noexcept {
		buffer.swap(move_buffer.buffer);
		element_size = move_buffer.element_size;
		actual_size = move_buffer.actual_size;
	}

	template <typename ElementType>
	ElementType* Buffer<ElementType>::data() {
		return buffer.get();
	}

	template <typename ElementType>
	void Buffer<ElementType>::reserve(uint64_t size) {
		if (actual_size < size) {
			allocate(size);
		}
	}

	template <typename ElementSize>
	uint64_t Buffer<ElementSize>::size() const {
		return element_size;
	}

	template <typename ElementSize>
	void Buffer<ElementSize>::set_size(uint64_t size) {
		if (actual_size < size) {
			throw BufferException("New size of buffer bigger than actual size!");
		}

		element_size = size;
	}

	template <typename ElementSize>
	bool Buffer<ElementSize>::empty() const {
		return !element_size;
	}

	template <typename ElementSize>
	ElementSize* Buffer<ElementSize>::begin() {
		return buffer.get();
	}

	template <typename ElementType>
	const ElementType* Buffer<ElementType>::cbegin() const {
		return buffer.get();
	}

	template <typename ElementType>
	ElementType* Buffer<ElementType>::end() {
		return buffer.get() + element_size;
	}

	template <typename ElementType>
	const ElementType* Buffer<ElementType>::cend() const {
		return buffer.get() + element_size;
	}

	template <typename ElementType>
	void Buffer<ElementType>::append(const ElementType* cbegin, const ElementType* cend) {
		uint64_t distance = cend - cbegin;
		reserve(distance);
		std::memcpy(buffer.get() + element_size, cbegin, distance);
		element_size += distance;
	}

	template <typename ElementType>
	void Buffer<ElementType>::swap(Buffer<ElementType>& swap_buffer) {
		buffer.swap(swap_buffer.buffer);
		std::swap(element_size, swap_buffer.element_size);
		std::swap(actual_size, swap_buffer.actual_size);
	}
}


#endif //SERVER_BUFFER_HPP