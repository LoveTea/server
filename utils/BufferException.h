//
// Created by kirill on 16.11.17.
//

#ifndef SERVER_UTILSEXCEPTION_H
#define SERVER_UTILSEXCEPTION_H


#include "BaseException.h"

namespace utils {
	class BufferException : public BaseException {
	public:
		explicit BufferException(const std::string& message) : BaseException(message) {}
	};
}


#endif //SERVER_UTILSEXCEPTION_H
