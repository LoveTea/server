//
// Created by kirill on 18.11.17.
//

#include <algorithm>

#include "Strings.h"

namespace utils {
	void Strings::ltrim(std::string& string) {
		string.erase(string.begin(), std::find_if(string.begin(), string.end(), [](char c) -> bool {
			return !std::isspace(c);
		}));
	}

	void Strings::rtrim(std::string& string) {
		string.erase(std::find_if(string.rbegin(), string.rend(), [](char c) -> bool {
			return c != ' ';
		}).base(), string.end());
	}

	void Strings::trim(std::string& string) {
		ltrim(string);
		rtrim(string);
	}

	void Strings::to_lower(std::string& string) {
		std::transform(string.cbegin(), string.cend(), string.begin(), [](unsigned char c) {
			return std::tolower(c);
		});
	}

	std::string Strings::to_lower_copy(const std::string& string) {
		std::string result(string.size(), 0);
		std::transform(string.cbegin(), string.cend(), result.begin(), [](unsigned char c) {
			return std::tolower(c);
		});

		return result;
	}
}