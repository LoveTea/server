//
// Created by kirill on 18.11.17.
//

#ifndef SERVER_STRINGS_H
#define SERVER_STRINGS_H

#include <string>

namespace utils {
	class Strings {
	public:
		static void ltrim(std::string& string);
		static void rtrim(std::string& string);
		static void trim(std::string& string);

		static void to_lower(std::string& string);
		static std::string to_lower_copy(const std::string& string);
	};
}


#endif //SERVER_STRINGS_H
